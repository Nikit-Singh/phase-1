## Phase 1 project

Use the following command to run the application in local environment.
```sh
npm run dev
```

>I have used Solana devnet while building and testing this app as I don't own any NFTs on the mainnet, but this app should more or less work on mainnet too. Just make the change in `/src/utils/connection.js` file to run on different environments.

## Tech stack used
- Next.js
- TailwindCSS
- Jotai for state management
- Solana wallet adapter for wallet integration
- Metaplex JS SDK