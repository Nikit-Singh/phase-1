import { useAtom } from "jotai";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useWallet } from "@solana/wallet-adapter-react";

import Layout from "@/src/layout/Layout";
import { user_assets } from "@/src/store";
import getMetadata from "@/src/utils/getMetadata";

const Collection = () => {
  const router = useRouter();
  const { id } = router.query;

  const { publicKey } = useWallet();

  const [userAssets, setUserAssets] = useAtom(user_assets);

  const [collection, setCollection] = useState([]);
  const [collectionDetails, setCollectionDetails] = useState([]);
  const [loading, setLoading] = useState(true);

  async function fetchMetadata() {
    publicKey && (await setUserAssets(await getMetadata(publicKey)));
  }

  async function fetchAssetCollection() {
    setCollection([]);
    const newCollection = [];

    userAssets?.forEach((asset) => {
      if (asset.data.symbol === id) {
        newCollection.push(asset);
      }
    });

    setCollection(newCollection);
  }

  async function fetchCollectionDetails() {
    const mappedUris = collection?.map(async (asset) => {
      const res = await fetch(asset.data.uri);
      const data = await res.json();
      return data;
    });

    const newCollectionDetails = await Promise.all(mappedUris);
    setCollectionDetails(newCollectionDetails);
    setLoading(false);
  }

  useEffect(() => {
    setCollection([]);
    fetchMetadata();
  }, [publicKey]);

  useEffect(() => {
    fetchAssetCollection();
  }, [JSON.stringify(userAssets), publicKey]);

  useEffect(() => {
    setCollectionDetails([]);
    fetchCollectionDetails();
  }, [JSON.stringify(collection)]);

  if (!publicKey) {
    return (
      <Layout>
        <div className="text-4xl font-bold flex justify-center items-center h-[90vh] w-full">
          Connect your wallet to see your NFTs.
        </div>
      </Layout>
    );
  }

  return (
    <Layout>
      <main className="mt-16">
        <h1 className="text-4xl font-bold mb-8">{id}</h1>
        <ul className="flex flex-col">
          {loading && <li>Loading...</li>}
          {!loading && collectionDetails.length === 0 && (
            <li>{`You don't own any nft in this collection.`}</li>
          )}
          {collectionDetails?.map((asset, index) => {
            return (
              <li className="my-4 p-4 flex bg-black/10" key={asset.name}>
                <img
                  className="h-48 w-48 rounded-sm"
                  src={asset.image}
                  alt={asset.name}
                />
                <div className="pl-4 -mt-1 flex flex-col space-y-2">
                  <span className="font-semibold text-2xl">{asset.name}</span>
                  <span>
                    Mint Address :{" "}
                    {collection[index]?.mint ||
                      "Unable to find the mint address"}
                  </span>
                </div>
              </li>
            );
          })}
        </ul>
      </main>
    </Layout>
  );
};

export default Collection;
