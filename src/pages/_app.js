import { Provider as JotaiProvider } from "jotai";

import WalletProvider from "@/src/layout/WalletProvider";

import "@/src/styles/globals.css";

function MyApp({ Component, pageProps }) {
  return (
    <JotaiProvider>
      <WalletProvider>
        <Component {...pageProps} />
      </WalletProvider>
    </JotaiProvider>
  );
}

export default MyApp;
