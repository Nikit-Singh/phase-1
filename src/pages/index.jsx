import { useAtom } from "jotai";
import { useEffect, useState } from "react";
import { useWallet } from "@solana/wallet-adapter-react";

import Layout from "../layout/Layout";
import getMetadata from "../utils/getMetadata";
import HomeNFTCard from "../components/HomeNFTCard";
import { user_assets, user_all_collections } from "../store";

const Home = () => {
  const { publicKey } = useWallet();

  const [_, setUserAssets] = useAtom(user_assets);
  const [collections] = useAtom(user_all_collections);

  const [loading, setLoading] = useState(true);
  const [collectionDetails, setCollectionDetails] = useState([]);

  async function fetchMetadata() {
    publicKey && (await setUserAssets(await getMetadata(publicKey)));
  }

  async function fetchCollectionDetails() {
    setLoading(true);
    const mappedUris = collections?.map(async (asset) => {
      const res = await fetch(asset.data.uri);
      const data = await res.json();
      return data;
    });

    const newCollectionDetails = await Promise.all(mappedUris);
    setCollectionDetails(newCollectionDetails);
    await setLoading(false);
  }

  useEffect(() => {
    setLoading(true);
    fetchMetadata();
    setLoading(false);
  }, [publicKey]);

  useEffect(() => {
    setCollectionDetails([]);
    fetchCollectionDetails();
  }, [JSON.stringify(collections)]);

  if (!publicKey) {
    return (
      <Layout>
        <div className="text-4xl font-bold flex justify-center items-center h-[90vh] w-full">
          Connect your wallet to see your NFTs.
        </div>
      </Layout>
    );
  }

  return (
    <Layout>
      <h1 className="text-4xl font-bold mt-16">Your NFT Collections</h1>
      <ul className="flex mt-4 space-x-4">
        {collectionDetails.length !== 0 ? (
          collectionDetails.map((asset) => {
            return <HomeNFTCard asset={asset} key={asset.name} />;
          })
        ) : (
          <li>
            {loading
              ? "Loading..."
              : "You have no NFTs in your current wallet."}
          </li>
        )}
      </ul>
    </Layout>
  );
};

export default Home;
