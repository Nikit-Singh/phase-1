import Link from "next/link";

const HomeNFTCard = ({ asset }) => {
  return (
    <li>
      <Link href={`/collection/${asset.symbol}`}>
        <a className="p-1 rounded-sm inline-block bg-gray-800">
          <img
            src={asset.image}
            alt={asset.name}
            className="h-48 w-48 rounded-sm"
          />
          <span className="font-medium">{asset.symbol}</span>
        </a>
      </Link>
    </li>
  );
};

export default HomeNFTCard;
