import {
  WalletDisconnectButton,
  WalletMultiButton,
} from "@solana/wallet-adapter-react-ui";
import { useWallet } from "@solana/wallet-adapter-react";

import { useEffect, useState } from "react";
import connection from "../utils/connection";
import Link from "next/link";

require("@solana/wallet-adapter-react-ui/styles.css");

const header = () => {
  const [loading, setLoading] = useState(0);
  const [balance, setBalance] = useState(0);
  const { publicKey } = useWallet();

  const fetchWalletBalance = async () => {
    setLoading(true);
    const wallet_balance =
      (publicKey && (await connection.getBalance(publicKey)) / 1e9) || 0;
    await setBalance(wallet_balance);
    setLoading(false);
  };

  useEffect(() => {
    fetchWalletBalance();
  }, [publicKey]);

  return (
    <nav className="flex flex-row justify-between">
      <Link href={`/`}>
        <a className="text-3xl font-black italic text-yellow-400">PHASE 1</a>
      </Link>
      <div className="flex space-x-4 items-center">
        <ul className="flex flex-col  items-end">
          {publicKey && (
            <span className="font-medium">Address: {publicKey.toBase58()}</span>
          )}
          {publicKey && (
            <span className="text-lg font-medium">
              <span>Balance:</span>{" "}
              {loading ? "Loading..." : `${balance.toFixed(3)} SOL`}
            </span>
          )}
        </ul>
        {publicKey ? <WalletDisconnectButton /> : <WalletMultiButton />}
      </div>
    </nav>
  );
};

export default header;
