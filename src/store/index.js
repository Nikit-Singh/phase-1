import { atom } from "jotai";

export const user_assets = atom([]);

export const unique_collection_ids = atom((get) => {
  const ids = new Set();
  get(user_assets)?.forEach((asset) => {
    ids.add(asset.data.symbol);
  });
  return ids;
});

export const user_all_collections = atom((get) => {
  const ids = new Set(get(unique_collection_ids));
  const unique_collections = [];

  get(user_assets)?.forEach((asset) => {
    if (ids.has(asset.data.symbol)) {
      unique_collections.push(asset);
      ids.delete(asset.data.symbol);
    }
  });

  return unique_collections;
});
