import { programs } from "@metaplex/js";
import connection from "./connection";

export default async function getMetadata(publicKey) {
  const {
    metadata: { Metadata },
  } = programs;

  return await Metadata.findDataByOwner(connection, publicKey);
}
